﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeatherApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeatherView : ContentView
    {
        public event EventHandler Click;

        public WeatherView()
        {
            InitializeComponent();
        }

        public void OnWeatherElementTapped(object sender, EventArgs args)
        {
            if(Click != null)
            {
                Click(sender, args);
            }
        }
    }
}