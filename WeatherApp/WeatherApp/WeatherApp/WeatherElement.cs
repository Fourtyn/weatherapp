﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

namespace WeatherApp
{
    public class WeatherElement
    {
        public string Time { get; set; }
        public string WeatherIconSource { get; set; }
        public int Temperature { get; set; }
        public int TemperatureLow { get; set; }
    }
}
