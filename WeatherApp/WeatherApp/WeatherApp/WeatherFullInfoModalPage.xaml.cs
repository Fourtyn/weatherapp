﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeatherApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeatherFullInfoModalPage : ContentPage
    {
        public WeatherViewModel CurrentWeather { get; set; }
        public WeatherFullInfoModalPage(WeatherViewModel currentWeather)
        {
            InitializeComponent();

            CurrentWeather = currentWeather;

            BindingContext = this;
        }
        private async void Close_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync(false);
        }
    }
}