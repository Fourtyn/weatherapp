﻿using System.Globalization;

namespace WeatherApp
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }
}
