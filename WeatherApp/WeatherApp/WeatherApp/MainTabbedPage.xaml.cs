﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeatherApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabbedPage : TabbedPage
    {
        private Page _previousPage;
        public MainTabbedPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnCurrentPageChanged()
        {
            // clear MainPage stack on switching tab
            if (_previousPage is NavigationPage && (_previousPage as NavigationPage).CurrentPage is WeatherFullInfoPage)
            {
                TabbedPage_CurrentPageChanged();
            }

            NavigationPage currNav = CurrentPage as NavigationPage;
            if (currNav != null)
            {
                Page specificCurrPage = currNav.CurrentPage;
                // disable Page3
                if (!(specificCurrPage is Page3))
                    _previousPage = CurrentPage;
                else
                {
                    CurrentPage = _previousPage;
                    base.OnCurrentPageChanged();
                }

                // restart drawing circular progress bar if opening that page
                // if closing c.p.bar page, stop drawing
                if(specificCurrPage is CircularProgressBarPage)
                {
                    (specificCurrPage as CircularProgressBarPage).RestartProgressBar();
                }

                // display toast message
                DependencyService.Get<IToast>().Show("Current page is " + currNav.CurrentPage.GetType().Name);
            }
        }

        private async void TabbedPage_CurrentPageChanged()
        {
            await _previousPage.Navigation.PopToRootAsync();
        }
    }
}