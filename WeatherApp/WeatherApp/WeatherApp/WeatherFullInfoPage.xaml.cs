﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeatherApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeatherFullInfoPage : ContentPage
    {
        public WeatherByDateViewModel CurrentWeather { get; set; }
        public WeatherFullInfoPage(WeatherByDateViewModel currentWeather)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            CurrentWeather = currentWeather;

            BindingContext = this;
        }
        public async void BackButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync(false);
        }
    }
}