﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp
{
    public interface IToast
    {
        void Show(string message);
    }
}
