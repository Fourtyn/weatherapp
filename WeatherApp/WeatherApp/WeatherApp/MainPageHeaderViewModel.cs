﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace WeatherApp
{
    public class MainPageHeaderViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private MainPageHeaderModel _model;

        public MainPageHeaderViewModel()
        {
            _model = new MainPageHeaderModel();
        }

        public string Town
        {
            get { return _model.Town; }
            set
            {
                if (_model.Town != value)
                {
                    _model.Town = value;
                    OnPropertyChanged("Town");
                }
            }
        }
        public string WeatherATM
        {
            get { return _model.WeatherATM; }
            set
            {
                if (_model.WeatherATM != value)
                {
                    _model.WeatherATM = value;
                    OnPropertyChanged("WeatherATM");
                }
            }
        }
        public int TempATMValue
        {
            get { return _model.TempATMValue; }
            set
            {
                if (_model.TempATMValue != value)
                {
                    _model.TempATMValue = value;
                    OnPropertyChanged("TempATMValue");
                }
            }
        }
        public string TemperatureATM
        {
            get { return _model.TemperatureATM; }
            set
            {
                if (_model.TemperatureATM != value)
                {
                    _model.TemperatureATM = value;
                    OnPropertyChanged("TemperatureATM");
                }
            }
        }

        public string DayOfTheWeek
        {
            get { return _model.DayOfTheWeek; }
            set
            {
                if (_model.DayOfTheWeek != value)
                {
                    _model.DayOfTheWeek = value;
                    OnPropertyChanged("DayOfTheWeek");
                }
            }
        }
        public string TodayStr
        {
            get { return _model.TodayStr; }
            set
            {
                if (_model.TodayStr != value)
                {
                    _model.TodayStr = value;
                    OnPropertyChanged("TodayStr");
                }
            }
        }
        public int TodayTempMax
        {
            get { return _model.TodayTempMax; }
            set
            {
                if (_model.TodayTempMax != value)
                {
                    _model.TodayTempMax = value;
                    OnPropertyChanged("TodayTempMax");
                }
            }
        }
        public int TodayTempMin
        {
            get { return _model.TodayTempMin; }
            set
            {
                if (_model.TodayTempMin != value)
                {
                    _model.TodayTempMin = value;
                    OnPropertyChanged("TodayTempMin");
                }
            }
        }
        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
