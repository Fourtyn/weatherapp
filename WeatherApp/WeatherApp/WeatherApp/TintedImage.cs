﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;

namespace WeatherApp
{
    public class TintedImage : Image
    {
        public static readonly BindableProperty TintModeProperty = BindableProperty.Create(nameof(TintMode), typeof(TintModes), typeof(TintedImage), TintModes.Multiply);
        public static readonly BindableProperty TintColorProperty = BindableProperty.Create(nameof(TintColor), typeof(Color), typeof(TintedImage), Color.Black);

        public enum TintModes
        {
            Multiply,
            SrcIn,
            SrcAtop,
            Src,
            Screen,
            Overlay,
            Xor,
            Lighten,
            SrcOut,
            DstOver,
            DstIn,
            DstAtop,
            Dst,
            Darken,
            Clear,
            Add,
            DstOut,
            SrcOver
        }

        public TintModes TintMode 
        {
            set
            {
                SetValue(TintModeProperty, value);
            }
            get
            {
                return (TintModes)GetValue(TintModeProperty);
            }
        }
        public Color TintColor
        {
            set
            {
                SetValue(TintColorProperty, value);
            }
            get
            {
                return (Color)GetValue(TintColorProperty);
            }
        }
    }
}
