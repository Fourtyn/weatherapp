﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp
{
    public class MainPageHeaderModel
    {
        public string GradSymbol = "°";
        public string Town { get; set; } = LanguageResource.Town;
        public string WeatherATM { get; set; } = LanguageResource.WeatherATM;
        public int TempATMValue { get; set; } = 54;
        public string TemperatureATM { get; set; }
        public string DayOfTheWeek { get; set; } = LanguageResource.DayOfTheWeek4;
        public string TodayStr { get; set; } = LanguageResource.TodayStr;
        public int TodayTempMax { get; set; } = 81;
        public int TodayTempMin { get; set; } = 52;

        public MainPageHeaderModel()
        {
            TemperatureATM = TempATMValue.ToString() + GradSymbol;
        }

    }
}
