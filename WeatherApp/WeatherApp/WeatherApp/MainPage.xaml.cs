﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WeatherApp
{
    public enum WeatherEnum
    {
        Sunny = 0,
        Cloudy = 1
    }
    public partial class MainPage : ContentPage
    {
        public List<string> IconPaths { get; set; } = new List<string> { "sun.png", "cloud.png" };
        public List<WeatherElement> WeatherByTime { get; set; }
        public List<WeatherViewModel> WeatherByTimeViewModels { get; set; }

        public List<WeatherElement> WeatherByDate { get; set; }
        public List<WeatherByDateViewModel> WeatherByDateViewModels { get; set; }
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            WeatherByTimeViewModels = new List<WeatherViewModel>
            {
                new WeatherViewModel {Time = LanguageResource.TimeNowStr, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 50 },
                new WeatherViewModel {Time = "6AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 51 },
                new WeatherViewModel {Time = "7AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 54 },
                new WeatherViewModel {Time = "8AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 55 },
                new WeatherViewModel {Time = "9AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 56 },
                new WeatherViewModel {Time = "10AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 57 },
                new WeatherViewModel {Time = "16AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 51 },
                new WeatherViewModel {Time = "17AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 54 },
                new WeatherViewModel {Time = "18AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 55 },
                new WeatherViewModel {Time = "19AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 56 },
                new WeatherViewModel {Time = "20AM", WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 57 },
            };

            WeatherByDateViewModels = new List<WeatherByDateViewModel>
            {
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek5, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 82, TemperatureLow = 52 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek6, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 73, TemperatureLow = 52 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek7, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 75, TemperatureLow = 54 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek1, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 73, TemperatureLow = 54 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek2, WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 73, TemperatureLow = 52 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek3, WeatherIconSource = IconPaths[(int)WeatherEnum.Sunny], Temperature = 82, TemperatureLow = 52 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek4, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 73, TemperatureLow = 52 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek5, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 75, TemperatureLow = 54 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek6, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 73, TemperatureLow = 54 },
                new WeatherByDateViewModel {Time = LanguageResource.DayOfTheWeek7, WeatherIconSource = IconPaths[(int)WeatherEnum.Cloudy], Temperature = 73, TemperatureLow = 52 },
            };

            BindingContext = this;
        }

        public void ListView_ItemTapped(object sender, EventArgs e)
        {
            ItemTappedEventArgs args = e as ItemTappedEventArgs;

            WeatherByDateViewModel selectedWeather = args.Item as WeatherByDateViewModel;
            ToWeatherFullInfoPage(selectedWeather);
        }
        public async void ToWeatherFullInfoModalPage(WeatherFullInfoModalPage modalPage)
        {
            await Navigation.PushModalAsync(modalPage, false);
        }

        public async void ToWeatherFullInfoPage(WeatherByDateViewModel selectedWeather)
        {
            if (selectedWeather != null)
            {
                await Navigation.PushAsync(new WeatherFullInfoPage(selectedWeather), false);
            }
            else
                await DisplayAlert("Error", "Null WeatherVM!", "X");
        }

        private void WeatherView_Click(object sender, EventArgs args)
        {
            WeatherViewModel selectedWeather = ((Grid)sender).BindingContext as WeatherViewModel;
            if (selectedWeather != null)
            {
                WeatherFullInfoModalPage modalPage = new WeatherFullInfoModalPage(selectedWeather);
                ToWeatherFullInfoModalPage(modalPage);
            }
        }
    }
}