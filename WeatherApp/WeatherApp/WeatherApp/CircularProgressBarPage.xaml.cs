﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.ComponentModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SkiaSharp;
using SkiaSharp.Views.Forms;

namespace WeatherApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CircularProgressBarPage : ContentPage, INotifyPropertyChanged
    {
        private const float _startAngle = -90.0f;

        public event PropertyChangedEventHandler PropertyChanged;
        public int CycleTime = 10000; // in ms

        private float _sweepAngle = 0.0f;
        private float _pbArcWidth = 20.0f;
        private bool _pageIsActive;
        private bool _drawingFinished = false;
        private Stopwatch _stopwatch = new Stopwatch();
        private int _percentage;
        private string _percentageStr;

        public int Percentage
        {
            get { return _percentage; }
            set
            {
                if (_percentage != value)
                {
                    _percentage = value;
                    OnPropertyChanged("Percentage");
                }
            }
        }

        public CircularProgressBarPage()
        {
            InitializeComponent();

            BindingContext = this;
        }

        public async void RestartProgressBar()
        {
            _sweepAngle = 0;
            _drawingFinished = false;
            await AnimationLoop();
        }
        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();

            SKPaint arcPaint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = Color.Red.ToSKColor(),
                StrokeWidth = _pbArcWidth
            };
            SKPaint bgArcPaint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = Color.Gray.ToSKColor(),
                StrokeWidth = _pbArcWidth
            };
            SKPaint textPaint = new SKPaint
            {
                Color = SKColors.Black
            };

            // set parameters of circles of progress bar
            int sizeH = info.Size.Height;
            int sizeW = info.Size.Width;

            int borderMargin = sizeH > sizeW ? sizeW / 10 : sizeH / 10;
            int diameter = sizeH > sizeW ? sizeW - 2 * borderMargin : sizeH - 2 * borderMargin;
            float rectXMin = borderMargin;
            float rectYMin = borderMargin;

            SKPoint circleCenter = new SKPoint
            {
                X = rectXMin + diameter / 2,
                Y = rectYMin + diameter / 2
            };

            SKRect rect = new SKRect(rectXMin, rectYMin, rectXMin + diameter, rectYMin + diameter);

            // draw circles
            canvas.DrawCircle(circleCenter, diameter / 2, bgArcPaint);
            using (SKPath pbPath = new SKPath())
            {
                pbPath.AddArc(rect, _startAngle, _sweepAngle);
                canvas.DrawPath(pbPath, arcPaint);
            }

            // TEXT
            _percentageStr = Percentage.ToString() + '%';

            // Adjust TextSize property so text is 40% of diameter
            float textWidth = textPaint.MeasureText(_percentageStr);
            textPaint.TextSize = 0.4f * diameter * textPaint.TextSize / textWidth;
            if (Percentage < 10)
                textPaint.TextSize = textPaint.TextSize * 2.0f / 3.0f;

            // Find the text bounds
            SKRect textBounds = new SKRect();
            textPaint.MeasureText(_percentageStr, ref textBounds);

            // Calculate offsets to center the text on the screen
            float xText = rectXMin + diameter / 2 - textBounds.MidX;
            float yText = rectYMin + diameter / 2 - textBounds.MidY;

            // And draw the text
            canvas.DrawText(_percentageStr, xText, yText, textPaint);

        }
        private async Task AnimationLoop()
        {
            _stopwatch.Start();

            while (_pageIsActive && !_drawingFinished)
            {
                _sweepAngle = ((float)_stopwatch.Elapsed.TotalMilliseconds * 360 / CycleTime);

                Percentage = (int)((_sweepAngle * 100) / 360);
                if (Percentage > 100)
                    Percentage = 100;

                canvasView.InvalidateSurface();

                if (_sweepAngle >= 360)
                    _drawingFinished = true;

                await Task.Delay(TimeSpan.FromSeconds(1.0f / 30.0f));
            }

            _stopwatch.Stop();
            _stopwatch.Reset();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _pageIsActive = true;
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _pageIsActive = false;
        }
        protected override void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}