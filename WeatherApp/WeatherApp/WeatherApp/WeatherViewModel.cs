﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
using System.ComponentModel;

namespace WeatherApp
{
    public class WeatherViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private WeatherElement _weather;

        public WeatherViewModel()
        {
            _weather = new WeatherElement();
        }

        public string Time
        {
            get { return _weather.Time; }
            set
            {
                if (_weather.Time != value)
                {
                    _weather.Time = value;
                    OnPropertyChanged("Time");
                }
            }
        }

        public string WeatherIconSource
        {
            get { return _weather.WeatherIconSource; }
            set
            {
                if (_weather.WeatherIconSource != value)
                {
                    _weather.WeatherIconSource = value;
                    OnPropertyChanged("WeatherIconSource");
                }
            }
        }

        public int Temperature
        {
            get { return _weather.Temperature; }
            set
            {
                if (_weather.Temperature != value)
                {
                    _weather.Temperature = value;
                    OnPropertyChanged("Temperature");
                }
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
