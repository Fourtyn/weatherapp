﻿using Android.Graphics;

namespace WeatherApp.Droid
{
    public class TintedImageToPorterDuffModeConverter
    {
        public static PorterDuff.Mode ConvertTintToPD(TintedImage.TintModes tintMode)
        {
            switch (tintMode)
            {
                case TintedImage.TintModes.Multiply:
                    return PorterDuff.Mode.Multiply;
                case TintedImage.TintModes.SrcIn:
                    return PorterDuff.Mode.SrcIn;
                case TintedImage.TintModes.SrcAtop:
                    return PorterDuff.Mode.SrcAtop;
                case TintedImage.TintModes.Src:
                    return PorterDuff.Mode.Src;
                case TintedImage.TintModes.Screen:
                    return PorterDuff.Mode.Screen;
                case TintedImage.TintModes.Overlay:
                    return PorterDuff.Mode.Overlay;
                case TintedImage.TintModes.Xor:
                    return PorterDuff.Mode.Xor;
                case TintedImage.TintModes.Lighten:
                    return PorterDuff.Mode.Lighten;
                case TintedImage.TintModes.SrcOut:
                    return PorterDuff.Mode.SrcOut;
                case TintedImage.TintModes.DstOver:
                    return PorterDuff.Mode.DstOver;
                case TintedImage.TintModes.DstIn:
                    return PorterDuff.Mode.DstIn;
                case TintedImage.TintModes.DstAtop:
                    return PorterDuff.Mode.DstAtop;
                case TintedImage.TintModes.Dst:
                    return PorterDuff.Mode.Dst;
                case TintedImage.TintModes.Darken:
                    return PorterDuff.Mode.Darken;
                case TintedImage.TintModes.Clear:
                    return PorterDuff.Mode.Clear;
                case TintedImage.TintModes.Add:
                    return PorterDuff.Mode.Add;
                case TintedImage.TintModes.DstOut:
                    return PorterDuff.Mode.DstOut;
                case TintedImage.TintModes.SrcOver:
                    return PorterDuff.Mode.SrcOver;
                default:
                    throw new System.Exception("Unexpected value of tint mode!");
            }
        }
    }
}